from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import json, requests
from mysite.microservice_config import API_PREGUNTAS_BASE_URL

@login_required
def question_list(request):
    if request.user.channelsuser.has_controls:
        return render(request, 'question_editor/question_list.html', {'base_api_url': API_PREGUNTAS_BASE_URL})
    return redirect(reverse('push:presentation_list'))


@login_required
def new(request):
    if request.user.channelsuser.has_controls:
        return render(request, 'question_editor/add_or_edit.html', {'id': 0, 'user_id': request.user.id,
                                                                    'base_api_url': API_PREGUNTAS_BASE_URL})
    return redirect(reverse('push:presentation_list'))


@login_required
def edit(request):
    if request.user.channelsuser.has_controls:
        id = int(request.GET.get('id', 0))
        return render(request, 'question_editor/add_or_edit.html', {'id': id, 'user_id': request.user.id,
                                                                    'base_api_url': API_PREGUNTAS_BASE_URL})
    return redirect(reverse('push:presentation_list'))


@csrf_exempt
@login_required
def add_or_edit(request):
    if request.user.channelsuser.has_controls and request.is_ajax():
        json_data = json.loads(request.body)
        id = int(request.GET.get('id', 0))
        if id <= 0:
            r = requests.post('%spregunta' % API_PREGUNTAS_BASE_URL, json=json_data)
        else:
            r = requests.put('%spregunta/%s' % (API_PREGUNTAS_BASE_URL, id), json=json_data)
        return JsonResponse(r.json())
    return redirect(reverse('push:presentation_list'))

@csrf_exempt
@login_required
def delete(request):
    if request.user.channelsuser.has_controls:
        id = int(request.GET.get('id', 0))
        if id > 0:
            r = requests.delete('%spregunta/%s' % (API_PREGUNTAS_BASE_URL, id))
        return JsonResponse(r.json())
    return redirect(reverse('push:presentation_list'))
