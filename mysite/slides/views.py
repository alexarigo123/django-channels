from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt
from mysite.microservice_config import API_PREGUNTAS_BASE_URL
from mysite.microservice_config import API_DIAPOSITIVAS_BASE_URL

@xframe_options_exempt
def index(request):
    id = int(request.GET.get('id', 0))
    slide_type = request.GET.get('type', '')

    if id > 0:
        if slide_type == 'question':
            if request.user.is_authenticated:
                return render(request, 'slides/question.html', {'id': id, 'base_api_url': API_PREGUNTAS_BASE_URL,
                                                                'is_teacher': request.user.channelsuser.has_controls})
            else:
                return render(request, 'slides/question_no_user.html', {})
        elif slide_type == 'slide':
            return render(request, 'slides/slide.html', {'id': id, 'base_api_url': API_DIAPOSITIVAS_BASE_URL})
    return render(request, 'slides/slide_not_found.html', {})
