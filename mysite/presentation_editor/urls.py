from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.presentation_list, name='list'),
    url(r'^new$', views.new, name='new'),
    url(r'^edit$', views.edit, name='edit'),
    url(r'^add_or_edit$', views.add_or_edit, name='add_or_edit'),
    url(r'^delete$', views.delete, name='delete'),
]