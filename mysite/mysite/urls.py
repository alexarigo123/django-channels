from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^', include(('push.urls', 'push'), namespace='push')),
    url(r'^users/', include(('users.urls', 'users'), namespace='users')),
    url(r'^slides/', include(('slides.urls', 'slides'), namespace='slides')),
    url(r'^presentation_editor/', include(('presentation_editor.urls', 'presentation_editor'),
                                          namespace='presentation_editor')),
    url(r'^question_editor/', include(('question_editor.urls', 'question_editor'),
                                      namespace='question_editor')),
    url(r'^admin/', admin.site.urls),
]