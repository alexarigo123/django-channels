from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
import json
from mysite.microservice_config import API_DIAPOSITIVAS_BASE_URL

def presentation_list(request):
    return render(request, 'push/presentation_list.html', {'base_api_url': API_DIAPOSITIVAS_BASE_URL})

def room(request, room_name):
    has_controls = False
    if request.user.is_authenticated:
        has_controls = request.user.channelsuser.has_controls

    return render(request, 'push/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name)),
        'has_controls': has_controls
    })
