from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.core.cache import cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
import json, requests
from mysite.microservice_config import API_PREGUNTAS_BASE_URL

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

class QuestionConsumer(WebsocketConsumer):
    def connect(self):
        user = self.scope["user"]
        if (user != 'AnonymousUser'):
            self.room_name = self.scope['url_route']['kwargs']['room_name']
            self.room_group_name = 'question_%s' % self.room_name

            # Join room group
            async_to_sync(self.channel_layer.group_add)(
                self.room_group_name,
                self.channel_name
            )

            self.accept()
            self.send_current_stage(user.channelsuser.has_controls)

    def send_current_stage(self, is_teacher=False):
        correct_answers_key = '%s_correct_answers' % self.room_group_name
        incorrect_answers_key = '%s_incorrect_answers' % self.room_group_name
        answers_key = '%s_answers' % self.room_group_name
        correct_answers = 0
        incorrect_answers = 0
        answers = []

        if is_teacher:
            cache.set(correct_answers_key, correct_answers, timeout=CACHE_TTL)
            cache.set(incorrect_answers_key, incorrect_answers, timeout=CACHE_TTL)
            cache.set(answers_key, answers, timeout=CACHE_TTL)
        else:
            if correct_answers_key in cache:
                correct_answers = cache.get(correct_answers_key)
            if incorrect_answers_key in cache:
                incorrect_answers = cache.get(incorrect_answers_key)
            if answers_key in cache:
                answers = cache.get(answers_key)

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'question_message',
                'message': {
                    'answers': answers,
                    'correctAnswers': correct_answers,
                    'incorrectAnswers': incorrect_answers
                }
            }
        )

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        user = self.scope["user"]
        text_data_json = json.loads(text_data)
        correct_answer = text_data_json['correctAnswer']
        question_id = text_data_json['questionId']
        correct_answers = 0
        incorrect_answers = 0
        answers = []

        requests.post('%sestadistica' % API_PREGUNTAS_BASE_URL, json={'id_pregunta': question_id,
                                                                      'correctas': 1 if correct_answer else 0,
                                                                      'incorrectas': 0 if correct_answer else 1})

        correct_answers_key = '%s_correct_answers' % self.room_group_name
        incorrect_answers_key = '%s_incorrect_answers' % self.room_group_name
        answers_key = '%s_answers' % self.room_group_name

        if correct_answers_key in cache:
            correct_answers = cache.get(correct_answers_key)
        if incorrect_answers_key in cache:
            incorrect_answers = cache.get(incorrect_answers_key)
        if answers_key in cache:
            answers = cache.get(answers_key)

        if correct_answer:
            correct_answers += 1
        else:
            incorrect_answers += 1
        answers.append({'username': user.username, 'correct_answer': correct_answer})

        cache.set(correct_answers_key, correct_answers, timeout=CACHE_TTL)
        cache.set(incorrect_answers_key, incorrect_answers, timeout=CACHE_TTL)
        cache.set(answers_key, answers, timeout=CACHE_TTL)

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'question_message',
                'message': {
                    'answers': answers,
                    'correctAnswers': correct_answers,
                    'incorrectAnswers': incorrect_answers
                }
            }
        )


    # Receive message from room group
    def question_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps(message))