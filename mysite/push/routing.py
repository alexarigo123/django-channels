from django.conf.urls import url

from . import presentation_consumer
from . import question_consumer

websocket_urlpatterns = [
    url(r'^ws/presentation/(?P<room_name>[^/]+)/$', presentation_consumer.PresentationConsumer),
    url(r'^ws/question/(?P<room_name>[^/]+)/$', question_consumer.QuestionConsumer),
]