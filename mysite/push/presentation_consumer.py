from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.core.cache import cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
import json, requests
from mysite.microservice_config import API_DIAPOSITIVAS_BASE_URL

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

class PresentationConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'presentation_%s' % self.room_name
        pages_key = '%s_pages' % self.room_group_name
        if pages_key not in cache:
            pages = self.get_pages_by_presentation_id(self.room_name)
            cache.set(pages_key, pages, timeout=CACHE_TTL)

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()
        self.send_current_stage()


    def get_pages_by_presentation_id(self, presentation_id):
        pages = []
        r = requests.get('%spresentacion/%s/all' % (API_DIAPOSITIVAS_BASE_URL, presentation_id))
        if r.status_code == requests.codes.ok:
            diapositivas = sorted(r.json()['diapositivas'], key=lambda k: k['orden'])
            for diapositiva in diapositivas:
                if diapositiva['tipo'] == 'slide':
                    pages.append('/slides?type=slide&id=%s' % diapositiva['id'])
                elif diapositiva['tipo'] == 'question':
                    pages.append('/slides?type=question&id=%s' % diapositiva['contenido'])

        if len(pages) == 0:
            pages.append('/slides?type=slide&id=0')

        return pages


    def send_current_stage(self):
        current_index_key = '%s_current_index' % self.room_group_name
        pages_key = '%s_pages' % self.room_group_name
        current_index = 0
        pages = ['/slides?type=slide&id=0']

        if current_index_key in cache:
            current_index = cache.get(current_index_key)
        if pages_key in cache:
            pages = cache.get(pages_key)
        current_url = pages[current_index]

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'presentation_message',
                'message': {
                    'currentUrl': current_url,
                    'hasPrevious': (current_index > 0),
                    'hasNext': (len(pages) > current_index + 1),
                    'currentIndex': current_index
                }
            }
        )


    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )


    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        current_index = text_data_json['currentIndex']
        action = text_data_json['action']
        pages_key = '%s_pages' % self.room_group_name
        pages = ['/slides?type=slide&id=0']

        if pages_key in cache:
            pages = cache.get(pages_key)

        if action == 'previous':
            current_index -= 1
        elif action == 'next':
            current_index += 1

        if len(pages) <= current_index + 1:
            current_index = len(pages) - 1
        elif current_index < 0:
            current_index = 0

        current_url = pages[current_index]

        cache.set('%s_current_index' % self.room_group_name, current_index, timeout=CACHE_TTL)

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'presentation_message',
                'message': {
                    'currentUrl': current_url,
                    'hasPrevious': (current_index > 0),
                    'hasNext': (len(pages) > current_index + 1),
                    'currentIndex': current_index
                }
            }
        )


    # Receive message from room group
    def presentation_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps(message))
