# chat/urls.py
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.presentation_list, name='presentation_list'),
    url(r'^push/$', views.presentation_list, name='presentation_list'),
    url(r'^push/presentation_list/$', views.presentation_list, name='presentation_list'),
    url(r'^push/presentation/(?P<room_name>[^/]+)/$', views.room, name='room'),
]