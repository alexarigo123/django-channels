from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.urls import reverse
from django.shortcuts import render, redirect

def log_in(request):
    form = AuthenticationForm()
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            return redirect(reverse('push:presentation_list'))
        else:
            print(form.errors)
    return render(request, 'users/log_in.html', {'form': form})

@login_required
def log_out(request):
    logout(request)
    return redirect(reverse('push:presentation_list'))

def sign_up(request):
    form = UserCreationForm()
    if request.method == 'POST':
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('users:log_in'))
    return render(request, 'users/sign_up.html', {'form': form})

def sign_up_teacher(request):
    form = UserCreationForm()
    if request.method == 'POST':
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            new_user = form.save(commit=True)
            print(new_user.channelsuser.has_controls)
            new_user.channelsuser.has_controls = True
            print(new_user.channelsuser.has_controls)
            new_user.save()
            return redirect(reverse('users:log_in'))
    return render(request, 'users/sign_up_teacher.html', {'form': form})